####################################################
#? General
####################################################
variable "resource_group_name" {
  type        = string
  description = "The Name which should be used for this Resource Group. Changing this forces a new Resource Group to be created."
}
variable "location" {
  type        = string
  description = "The Azure region where the resources should exist."
}

####################################################
#? Virtual Network
####################################################
variable "virtual_network_name" {
  type        = string
  description = "The name of the virtual network. Changing this forces a new resource to be created."
}
variable "virtual_network_address_space" {
  type        = list(string)
  description = "The address space that is used the virtual network. You can supply more than one address space."
}
variable "subnet_name" {
  type        = string
  description = "The name of the subnet. Changing this forces a new resource to be created."
}
variable "subnet_address_prefixes" {
  type        = list(string)
  description = "The address prefixes to use for the subnet."
}

####################################################
#? Azure Kubernetes Service
####################################################
variable "kubernetes_name" {
  type        = string
  description = "The name of the Managed Kubernetes Cluster to create. Changing this forces a new resource to be created."
}
variable "kubernetes_dns_prefix" {
  type        = string
  description = "DNS prefix specified when creating the managed cluster"
}
variable "kubernetes_version" {
  type        = string
  description = "Version of Kubernetes specified when creating the AKS managed cluster."
}
variable "kubernetes_sp_client_id" {
  type        = string
  description = "The Client ID for the Service Principal."
}
variable "kubernetes_sp_client_secret" {
  type        = string
  description = " The Client Secret for the Service Principal."
  sensitive   = true
}
variable "kubernetes_node_name" {
  type        = string
  description = "The name which should be used for the default Kubernetes Node Pool. Changing this forces a new resource to be created."
}
variable "kubernetes_node_count" {
  type        = number
  description = "The initial number of nodes which should exist in this Node Pool."
  default     = 1
}
variable "kubernetes_node_vm_size" {
  type        = string
  description = "The size of the Virtual Machine for the Kubernetes Node Pool"
}

####################################################
#? Azure Key Vault
####################################################
variable "kv_name" {
  type        = string
  description = "Specifies the name of the Key Vault."
}
variable "kv_soft_delete_retention_days" {
  type        = number
  description = "The number of days that items should be retained for once soft-deleted."
  default     = 7
}
variable "kv_sku_name" {
  type        = string
  description = "The Name of the SKU used for this Key Vault."
  default     = "standard"
}

####################################################
#? Azure Database Postgresql
####################################################
variable "psql_name" {
  type        = string
  description = "Specifies the name of the PostgreSQL Server. Changing this forces a new resource to be created."
}
variable "psql_administrator_user" {
  type        = string
  description = "The Administrator Login for the PostgreSQL Server."
}
variable "psql_administrator_password" {
  type        = string
  description = "The Password associated with the 'postgresql_administrator_user' for the PostgreSQL Server"
}
variable "psql_sku_name" {
  type        = string
  description = "Specifies the SKU Name for this PostgreSQL Server."
}
variable "psql_version" {
  type        = string
  description = "Specifies the version of PostgreSQL to use."
  default     = "13"
}
variable "psql_storage_mb" {
  type        = number
  description = "Max storage allowed for a server."
  default     = 32768
}
variable "psql_backup_retention_days" {
  type        = number
  description = "Backup retention days for the server, supported values are between 7 and 35 days."
  default     = 7
}
