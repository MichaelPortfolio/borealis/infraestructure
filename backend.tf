terraform {
  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "MichaelPortfolio"
    workspaces {
      name = "Borealis"
    }
  }
  required_version = ">= 1.3.4"
  required_providers {
    azurerm = {
      version = ">= 3.31.0"
      source  = "hashicorp/azurerm"
    }
  }
}